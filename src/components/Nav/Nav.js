import React from 'react';
import { Link } from 'react-router-dom'
import SearchBar from '../SearchBar/SearchBarContainer';

import styled from '@emotion/styled'

var white = "#fefefe";
var black = "#2d3436";
var red = "#d63031";
var grey = "#dfe6e9";

const TextLink = styled.h2`
    text-decoration:none;
    font-family: NetflixRegular;
    font-size: 1.6em;
    margin: 20px 10px;
    color: ${white};
    :hover {
      color: ${red};
    }
`

const NavContainer = styled.div`
    display: flex;
    width: 100%;
    justify-content: space-around;
    padding: 0px 0px;
    background-color: ${black};
    :hover {
      color: ${white};
    }

`

const Nav = ({handleClick}) => (
  <NavContainer className="nav">
    <div className="row middle-md" style={{justifyContent: 'space-around', width: '100%'}}>
      <div className="col-md-1 middle-md">
        <Link style={{ textDecoration: 'none'}} to="/">
          <TextLink>Home</TextLink>
        </Link>
      </div>
      <div className="col-md-8 center-md">
        <SearchBar/>
      </div>
      <div className="col-md-1 end-xs">
        <Link style={{ textDecoration: 'none'}} to="/list">
          <TextLink>List</TextLink>
        </Link>
      </div>
      <div className="col-md-2 end-xs">
        <Link style={{ textDecoration: 'none'}} to="/storybook">
          <TextLink>Storybook</TextLink>
        </Link>
      </div>
    </div>
  </NavContainer>
);

export default Nav;
