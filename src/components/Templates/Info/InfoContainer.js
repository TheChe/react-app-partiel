import React, { Component } from 'react';
import Info from './Info';
import api from '../../../helpers/api';
import localstorage from '../../../helpers/localstorage';

class InfoContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            id: ""
        };
    }
    async componentDidMount() {
        const data = await api.getMovieById(this.props.match.params.id);
        this.setState({
            data: data,
            id: this.props.match.params.id
        })
        console.log(data);
    }
    async componentDidUpdate() {
        if(this.props.match.params.id !== this.state.id){
            const data = await api.getMovieById(this.props.match.params.id);
            this.setState({
                data: data,
                id: this.props.match.params.id
            });
        }
    }


    handleClickLike = (e) => {
        e.preventDefault();
        if(!this.state.like){
            localstorage.addToLocalStorage('likeList',this.props.data);
            console.log('LIKE LIST :',localstorage.getLikeList());
            this.setState({
                colorLike: "#C03A2B",
                like: true
            });
        }
        else {
            localstorage.deleteFromLocalStorage('likeList',this.props.data);
            console.log('LIKE LIST :',localstorage.getLikeList());
            this.setState({
                colorLike: "grey",
                like: false
            });
        };
      };
      
      handleClickWatched = (e) => {
        e.preventDefault();
        if(!this.state.watched){
            localstorage.addToLocalStorage('watchedList',this.props.data);
            console.log('WATCHED LIST :',localstorage.getWatchedList());
            this.setState({
                colorWatched: "#014CB7",
                watched: true
            });
        }
        else {
            localstorage.deleteFromLocalStorage('watchedList',this.props.data);
            console.log('WATCHED LIST :',localstorage.getWatchedList());
            this.setState({
                colorWatched: "grey",
                watched: false
            });
        }
      };


    render() {
        return (
            <Info
                handleClickLike={this.handleClickLike}
                handleClickWatched={this.handleClickWatched}
                handleClick={this.handleClick}
                data={this.state.data}
            />
        );
    }
}

export default InfoContainer;
