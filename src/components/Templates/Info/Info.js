import React from 'react';
import Nav from '../../Nav/NavContainer';
import CardMovie from '../../CardMovie/CardMovieContainer'
import styled from '@emotion/styled'


const InfoTextContainer = styled.div`
  font-family: 'NetflixSans-Light';
  display: flex;
  flex-direction: column;
`

const Info = ({data, handleClickLike, handleClickWatched, handleClick}) => (  
  <div className="home">
      
      <Nav/>

    <div className="data-container">
      <CardMovie
            data={data}
            id={data.id}
            key={data.id}
            poster={data.Poster}
            title={data.title}
            rating={data.rating}
            genre={data.genre}
            handleClickLike={handleClickLike}
            handleClickWatched={handleClickWatched}
            colorLike={data.colorLike}
            colorWatched={data.colorWatched}
        />
        <InfoTextContainer className="col">
          <p className="col-md-12 center-md" style={{fontFamily: 'NetflixLight'}}>{data.Plot}</p>
          <span className="col-md-4 center-md" style={{fontFamily: 'NetflixLight'}}>Official Director : {data.Director}</span>
          <span className="col-md-8 center-md" style={{fontFamily: 'NetflixLight'}}>Actors : {data.Actors}</span>
        </InfoTextContainer>
      
    </div>
  </div>
);

export default Info;
