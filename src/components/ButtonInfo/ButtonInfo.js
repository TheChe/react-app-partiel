import React from 'react';
import styled from '@emotion/styled'

var white = "#fefefe";
var black = "#2d3436";
var red = "#d63031";
var grey = "#dfe6e9";

const Button = styled.button`
  margin: auto;
  color: ${black};
  border-radius: 34px;
  padding: 15px 50px;
  border: 2px solid ${black};
  background: ${white}; 
  cursor:pointer;
  :hover {
    color: ${red};
    border: 2px solid ${red};
  }
`
const TextButton = styled.span`
  font-family: NetflixBold;
  font-size: 24px;

`
const ButtonInfo = ({}) => (
  <div className="button-info">
    <Button>
      <TextButton>More Info</TextButton>
    </Button>
  </div>
);

export default ButtonInfo;
