import React from 'react';
import styled from '@emotion/styled'

var white = "#fefefe";
var black = "#2d3436";
var red = "#d63031";
var grey = "#dfe6e9";

const InputText = styled.input`
  border: none;
  padding: 10px;
  padding-left: 20px;
  margin: 10px;
  height: 60%;
  width: 80%;
  border: 2px solid ${black};
  border-radius: 5px;
  outline: none;
  :hover {
    border-color: ${red};
  }
  :focus {
    border-color:${red};
  }
`
const ButtonSearch = styled.button`
  position: relative;
  left: -56px;
  top: 1px;
  height: 35px;
  border: none;
  background: ${white};
  text-align: center;
  color: ${black};
  border-radius: 34px;
  cursor: pointer;
  font-size: 20px;
  :hover {
    color: ${red};
  }
`
const IconSearch = styled.i`

`

const SearchBar = ({handleSubmit, handleChange, value}) => (
  <div className="search-bar">
  <form onSubmit={handleSubmit}>
  <InputText 
    type="text" 
    value={value} 
    onChange={handleChange} 
    placeholder="Search Movie, Serie, Doc..."
    />
    <ButtonSearch type="submit">
      <IconSearch className="fa fa-search"/>
    </ButtonSearch>
  </form>        
  </div>
);

export default SearchBar;
